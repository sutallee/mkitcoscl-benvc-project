// The ITC compiler will generate the following
// files:
//	reqapi.h -
//		Contains request message descriptions;
//		the ITC server interface request() operations;
//		and other ITC server types.
//	api.h -
//		This is an optionally defined interface that
//		is generated if any of the messages specify the sync
//		keyword. This is not technically part of the ITC
//		interface, but is used to define the interface
//		to the concrete synchronous ITC class.
//	respapi.h -
//		Contains the response message descriptions and
//		the asynchronous ITC client interface response()
//		operations.
//	respmem.h -
//		Contains memory descriptions for all client
//		response messages and their payloads.
//	sync.h -
//		This header file describes a concrete class
//		that implements the (optional) interface Api
//		using synchronous ITC. This file is only
//		generated if there is at least one sync
//		keyword in a message description.
//	sync.h -
//		This C++ file describes a concrete class
//		that implements the (optional) interface Api
//		using synchronous ITC. This file is only
//		generated if there is at least one sync
//		keyword in a message description.

/**
	message	<MessageName> {
			There are three different keywords that are
			valid within a message declaration.
			fields -
				There may be exactly one fields declaration
				within a message body. The body of the
				fields declaration contains a description
				of data elements that are transfered
				between client and server.
			sync -
				Describes the name of a function and its
				return value that will be placed in the Api
				interface. The fields of the message payload
				described by other keywords will be mapped
				into the payload constructor, class fields
				and the Api interface arguments and return
					value. The first argument of the sync keyword
				names the operation. The second name after
				the keyword is the name of an inout or output
				field. The type of the inout or output field
				will become the return type of the operation. 
			cancel -
				This keyword will cause an additional
				message to be generated that is used
				to cancel the server request message.
				Cancellations are commonly used for
				notification services.

		fields {
				There are five different keywords that are
				valid within a fields declaration.
				input -
					Describes an argument which is used to pass
					information from the client to the server.
					The first field after the keyword is the
					type, and the second field is the base
					name of the field. The base name will
					be used as-is in the payload constructor
					and the Api interface (if any.) An underscore
					is prepended to the base name as the 
					field name in the payload.
				output -
					Describes an argument which is used to pass
					information from the server to the client.
					The first field after the keyword is the
					type, and the second field is the base
					name of the field. The base name will
					be used as-is in the payload constructor
					and the Api interface (if any.) An underscore
					is prepended to the base name as the 
					field name in the payload.
					Optionally, an "initial" value can be specified
					by adding an '=' assignment with the value to
					be placed in the initializer for this member.
				inout -
					Describes an argument which is used to pass
					information from the client to the server
					and then from the server to the client.
					The first field after the keyword is the
					type, and the second field is the base
					name of the field. The base name will
					be used as-is in the payload constructor
					and the Api interface (if any.) An underscore
					is prepended to the base name as the 
					field name in the payload.
				}

			}
 */

/** The include keyword specifies a C/C++ include file
	that will be included as a part of the request
	interface to make types available.
 */
include "oscl/mt/itc/whatnow.h";

namespace Oscl {
namespace Demo {

/** This is the DummyID ITC comment. */
itc DummyID {

	/** This message requests to the server to resolve
		all of the worlds problems.
	 */
	message	Resolve {
		/** There are three different keywords that are
			valid within a message declaration.
			fields -
				There may be exactly one fields declaration
				within a message body. The body of the
				fields declaration contains a description
				of data elements that are transfered
				between client and server.
			sync -
				Describes the name of a function and its
				return value that will be placed in the Api
				interface. The fields of the message payload
				described by other keywords will be mapped
				into the payload constructor, class fields
				and the Api interface arguments and return
					value. The first argument of the sync keyword
				names the operation. The second name after
				the keyword is the name of an inout or output
				field. The type of the inout or output field
				will become the return type of the operation. 
			cancel -
				This keyword will cause an additional
				message to be generated that is used
				to cancel the server request message.
				Cancellations are commonly used for
				notification services.
		 */
		fields{
			/** This field is filled in by the server to indicate
				whether or not is was able to resolve the worlds
				problems.
			 */
			output	bool					failed=true;

			/** This is a pointer to an empty buffer into which
				data from necessary for solving the worlds problems
				is written.
			 */
			input	unsigned char*			buffer;
			input	unsigned char*			xy;

			/** When the request is sent, the value of this this
				referenced field is the maximum number of octets
				that the buffer field can hold. When the resolve
				request completes successfully, it is updated
				to the actual number of octets written to the
				buffer.
			 */
			inout	unsigned&				length;
			}

		sync	bool resolve	failed;
		cancel;
		}

	/** This message requests the server to sign the
		document.
	 */
	message	Write {
		fields {
			/** This buffer contains the data to be
				written on the document.
			 */
			input	const void*		buffer;

			/** This is the number of octets in the
				buffer to be written to the document.
			 */
			inout	unsigned int	length;
			}
		sync	void write;
		}
	}
	
}
}
