############################################################
# Project root makefile
#

topdir=$(shell builddir=$(PWD); echo $${builddir%%/project/*};)

ifeq ($(toolroot),)
	toolroot=$(topdir)/tools/benvc
endif

srcroot=$(topdir)

ifeq ($(HOST),)
HOST:=$(shell uname)
endif

ifeq ($(HOSTMACH),)
HOSTMACH:=$(shell uname --machine)
endif

ifneq ($(HOST),)
        tooldir=$(toolroot)/toolset
        include $(tooldir)/host/$(HOST)
        include $(toolroot)/maketools/master
endif

